pip3 install requests pyyaml
mkdir -p ~/.git-templates/hooks
cp lint_checker.py ~/.git-templates/hooks/pre-commit
chmod u=rwx ~/.git-templates/hooks/pre-commit
git config --global init.templatedir '~/.git-templates'
