#!/usr/bin/env python3
"""
Validate .gitlab-ci.yml use the CI Lint API: https://docs.gitlab.com/ee/api/lint.html

TODO:
- Automatically remove .idea and venv. See https://github.com/tep/git-templates for ideas. And https://stackoverflow.com/questions/10840196/aborting-git-pre-commit-hook-when-var-dump-present.
- Add a bandit scan

Nick Livingston, July 2020
"""

import json
from pathlib import Path

import requests
import yaml


LINT_URL = 'https://gitlab.com/api/v4/ci/lint'
CI_PATH = Path.cwd().joinpath('.gitlab-ci.yml')


class bcolors:
    """https://stackoverflow.com/questions/287871/how-to-print-colored-text-in-python"""
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


if CI_PATH in Path.cwd().glob('*'):
    # Convert YAML to JSON
    payload = {'content': json.dumps(yaml.safe_load(CI_PATH.read_text()))}
    r = requests.post(LINT_URL, json=payload)
    response_json = r.json()
    if response_json['status'] == 'valid':
        print(f'{bcolors.OKGREEN}CI Lint indicates YAML is valid{bcolors.ENDC}')
    else:
        print(f'{bcolors.WARNING}CI Lint indicates YAML is invalid! Errors:\n{response_json["errors"]{bcolors.ENDC}')
        print(f'{bcolors.WARNING}Note: lint checker does not use authentication, use of includes may cause invalid access denied error{bcolors.ENDC}')
