# GitLab CI Lint Pre-Commit Hook

This project adds a pre-commit hook template to a git repository. The hook runs before each commit, and if the current directory contains a `.gitlab-ci.yml` file, it sends the contents of that file to the GitLab API for validation. The hook displays a message stating that either the YAML was valid, or what errors it contained.

To install this pre-commit hook in a templates directory so that all of your projects will use it, run the `install-precommit-hook.sh` script.
